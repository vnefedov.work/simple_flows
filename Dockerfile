FROM python:3.6

RUN mkdir /app
COPY . /app

WORKDIR /app
RUN export PYTHONPATH=.
RUN pip install -r requirements.txt

ENTRYPOINT ["sh", "-c", "python3 run.py $WORKER_MODE"]
