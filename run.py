import argparse
import logging

from src.controllers.activity import ActivityController
from src.controllers.base import BaseController
from src.controllers.decider import DeciderController
from src.controllers.execution import ExecutionController
from src.exceptions import ControllerExecutionError
from src.exceptions import ControllerSetupError
from src.flows.simple import SimpleFlow


def init_controllers():
    domain = 'dev'
    simple_flow = SimpleFlow('dev')

    return {
        'decider': DeciderController(simple_flow),
        'activity': ActivityController(simple_flow),
        'execution': ExecutionController(
            domain=domain, name=simple_flow.name,
            version=simple_flow.version, task_list=simple_flow.name)}


def init_parser(controller_names):
    parser = argparse.ArgumentParser(
        description='Start one of SWF workers or execution.')
    parser.add_argument(
        'mode', metavar='MODE', choices=controller_names,
        help='process mode (decider, activity, execution)')
    return parser


def main():
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger()

    controllers = init_controllers()

    parser = init_parser(list(controllers.keys()))
    args = parser.parse_args()

    controller_name = args.mode
    controller: BaseController = controllers.get(controller_name)
    if not controller:
        logger.critical('This mode is not available')
        exit(1)

    logger.info('Initializing instance...')
    try:
        controller.setup()
    except ControllerSetupError as error:
        logger.critical('Setup error: {}'.format(error.message))
        exit(1)

    logger.info('Executing instance in {} mode'.format(controller_name))
    try:
        controller.execute()
    except ControllerExecutionError as error:
        logger.critical('Execution error: {}'.format(error.message))


if __name__ == '__main__':
    main()
