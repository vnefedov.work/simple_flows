class ControllerError(Exception):
    message = ''

    def __init__(self, message):
        ControllerError.message = message


class ControllerExecutionError(ControllerError):
    pass


class ControllerSetupError(ControllerError):
    pass
