from garcon import task, runner
from garcon import activity


class SimpleFlow:
    def __init__(self, domain, name: str ='simple_flow'):
        self.domain = domain
        self.name = name
        self.version = '1.0'
        self.create_activity = activity.create(
            domain=self.domain, name=self.name)

    def decider(self, schedule, context=None):
        first_activity = schedule('first_activity', self.create_first_activity)
        # first_activity.wait()

        # schedule('second_activity', self.create_second_activity)

    @property
    def create_first_activity(self):
        def do_stuff(context, activity):
            print('Doing first activity')

        activity = self.create_activity(
            name='first_activity',
            run=runner.Sync(do_stuff))
        return activity

    @property
    def create_second_activity(self):
        def do_stuff(context, activity):
            print('Doing second activity')

        return self.create_activity(
            name='second_activity',
            run=runner.Sync(do_stuff))
