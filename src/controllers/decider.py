import time

from boto.exception import SWFResponseError
from garcon.decider import DeciderWorker

from src.controllers.base import BaseController
from src.exceptions import ControllerExecutionError
from src.exceptions import ControllerSetupError


class DeciderController(BaseController):
    def __init__(self, workflow, register=True):
        self.workflow = workflow
        self.register = register
        self.decider_worker: DeciderWorker = None

    def setup(self):
        try:
            self.decider_worker = DeciderWorker(
                self.workflow, register=self.register)
        except SWFResponseError as error:
            raise ControllerSetupError(error.error_message)

    def execute(self, *args, **kwargs):
        while True:
            try:
                self.decider_worker.run()
            except SWFResponseError as error:
                raise ControllerExecutionError(error.error_message)
            time.sleep(1)
