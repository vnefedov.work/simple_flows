class BaseController:
    def execute(self, *args, **kwargs):
        raise NotImplementedError('Abstract method call')

    def setup(self):
        pass

    def shutdown(self):
        pass
