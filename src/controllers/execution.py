from boto.swf import layer2 as swf

from src.controllers.base import BaseController


class ExecutionController(BaseController):
    def __init__(self, domain, name, version, task_list):
        self.domain = domain
        self.name = name
        self.version = version
        self.task_list = task_list

    def execute(self, *args, **kwargs):
        workflow_type = swf.WorkflowType(
            name=self.name, domain=self.domain,
            version=self.version, task_list=self.task_list)
        workflow_type.start()
