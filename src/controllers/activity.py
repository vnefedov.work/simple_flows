from threading import Thread

from boto.exception import SWFResponseError
from garcon.activity import ActivityWorker

from src.controllers.base import BaseController
from src.exceptions import ControllerSetupError
from src.exceptions import ControllerExecutionError


class ActivityController(BaseController):
    def __init__(self, workflow, activities: list = None):
        self.workflow = workflow
        self.activities = activities
        self.activity_worker: ActivityWorker = None

    def setup(self):
        try:
            self.activity_worker = ActivityWorker(self.workflow, self.activities)
        except SWFResponseError as error:
            raise ControllerSetupError(error.error_message)

    def execute(self, *args, **kwargs):
        try:
            thread = Thread(target=self.activity_worker.run)
            thread.start()
            thread.join()
        except SWFResponseError as error:
            raise ControllerExecutionError(error.error_message)
